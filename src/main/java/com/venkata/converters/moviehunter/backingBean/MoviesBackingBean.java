/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venkata.converters.moviehunter.backingBean;

import com.venkata.converters.moviehunter.dataBean.Movie;
import com.venkata.converters.moviehunter.DOM.DOMParser;
import com.venkata.converters.moviehunter.SAX.SAXParser;
import com.venkata.converters.moviehunter.StAX.StAXParser;
import java.io.FileNotFoundException;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.stream.XMLStreamException;
import org.w3c.dom.Document;

/**
 *
 * @author Venkata
 *
 * Class Controller to render the XML result from the DOM API parser
 */
@Named("BackingBean")
@RequestScoped
public class MoviesBackingBean implements Serializable {

    @Inject
    private Movie movie;

    private final Document doc = null;
    private String query;
    private String engine;
    private String message;
    private final String error;

    public MoviesBackingBean() {
        this.error = "Movie not available";
    }

    private static final String MOVIESXML = FacesContext.getCurrentInstance()
            .getExternalContext()
            .getRealPath("/resources/xml/movies.xml");
    private static final String MOVIESXSD = FacesContext.getCurrentInstance()
            .getExternalContext()
            .getRealPath("/resources/xml/moviesSchema.xsd");

    public void displayMovie() throws XMLStreamException, FileNotFoundException {

        switch (engine) {
            case "DOM":
                DOMParser dom = new DOMParser( movie,MOVIESXML,MOVIESXSD);
                if (dom.findMovieByTitle(query).isEmpty()
                        && dom.findMovieByActorName(query).isEmpty()
                        && dom.findMovieByDirectorName(query).isEmpty()) {
                    this.setMessage(error);
                }
                break;

            case "SAX":
                SAXParser sax = new SAXParser(MOVIESXML, movie);
                if (sax.findMovieByTitle(query).isEmpty()
                        && sax.findMovieByActorName(query).isEmpty()
                        && sax.findMovieByDirectorName(query).isEmpty()) {
                    this.setMessage(error);
                }
                break;

            case "StAX":
                StAXParser stax = new StAXParser(MOVIESXML, movie);
                if (stax.findMovieByTitle(query).isEmpty()
                        && stax.findMovieByActorName(query).isEmpty()
                        && stax.findMovieByDirectorName(query).isEmpty()) {
                    this.setMessage(error);
                }
                break;
           
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }
}
