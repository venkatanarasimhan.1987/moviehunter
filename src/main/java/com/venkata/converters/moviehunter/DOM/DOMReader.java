/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venkata.converters.moviehunter.DOM;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import com.venkata.converters.moviehunter.backingBean.MoviesBackingBean;

/**
 *
 * @author Venkata
 */
public class DOMReader {
    private Document document = null;
    private String xmlName = null;
    private String xsdName = null;
    private final StringBuilder xmlString;

    public DOMReader(String xmlName, String xsdName, StringBuilder xmlString) {
        this.xmlName = xmlName;
        this.xsdName = xsdName;
        this.xmlString = xmlString;        
    }
    /**
     * @return 
     * 
     * Return boolean if read Document Successfully.
     */
       private boolean readDocument() {

        boolean documentRead = false;
        try {
            
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();           
            DocumentBuilder documentBuilder;
            documentBuilder = dbf.newDocumentBuilder();
            
            document = documentBuilder.parse(new File(xmlName));
            documentRead = true;
        } catch (ParserConfigurationException ex) {
            xmlString.append("\nParserConfiguration error because ");
            xmlString.append(ex.getMessage());
        } catch (SAXException ex) {
            xmlString.append("\n").append(xmlName).append(" cannot be parsed because ");
            xmlString.append(ex.getMessage()).append("\n");
        } catch (IOException ex) {
            xmlString.append("\nI/O Exception because");
            xmlString.append(ex.getMessage());
        }

        return documentRead;
    }   
}
