/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venkata.converters.moviehunter.DOM;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import com.venkata.converters.moviehunter.dataBean.Movie;
import com.venkata.converters.moviehunter.backingBean.MoviesBackingBean;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Venkata
 */
@Dependent
public class DOMParser {
    
    private Document doc = null;

    private String xmlFile = null;
    private String xsdFile = null;
    private String message;
    private final Movie movie;

    public DOMParser(Movie movie, String xmlFile, String xsdFile) {
        this.xmlFile = xmlFile;
        this.movie = movie;
        this.xsdFile = xsdFile;

    }

    public boolean readDocument() {

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;
            db = dbf.newDocumentBuilder();

            doc = db.parse(new File(xmlFile));
            message = "true";
            return true;

        } catch (ParserConfigurationException ex) {
            message = "\nParserConfiguration error because ";
            message = ex.getMessage();
        } catch (SAXException ex) {
            message = "xml" + " cannot be parsed because ";
            message = ex.getMessage();
        } catch (IOException ex) {
            message = "\nI/O Exception because";
            message = ex.getMessage();
        }
        return false;
    }

    public boolean validSchema() {
        XMLSchemaValidator valid = new XMLSchemaValidator(xmlFile, xsdFile, message);
        return valid.validate();
    }

    /**
     * Load the XML query results into the Movie bean.
     *
     * @param movie
     * @param eElement
     * @return
     *
     */
    public Movie loadBean(Element eElement) {

        movie.setTitle(eElement.getElementsByTagName("title").item(0).getTextContent());
        movie.setRate(eElement.getElementsByTagName("rate").item(0).getTextContent());

        String[] split = eElement.getElementsByTagName("show_times").item(0).getTextContent().split("\\s+");
        movie.getTimesList().addAll(Arrays.asList(split));

        movie.setReleaseDate(eElement.getElementsByTagName("release_date").item(0).getTextContent());
        movie.setRunTime(eElement.getElementsByTagName("run_time").item(0).getTextContent());
        movie.setGenre(eElement.getElementsByTagName("genre").item(0).getTextContent());

        String actors = (eElement.getElementsByTagName("actors").item(0).getTextContent());
        movie.getActorsList().addAll(Arrays.asList(actors.split("\\s{2,2}")));

        String directors = eElement.getElementsByTagName("directors").item(0).getTextContent();
        movie.getDirectorsList().addAll(Arrays.asList(directors.split("\\s{2,2}")));

        String producers = eElement.getElementsByTagName("producers").item(0).getTextContent();
        movie.getProducersList().addAll(Arrays.asList(producers.split("\\s{2,2}")));

        String writers = eElement.getElementsByTagName("writers").item(0).getTextContent();
        movie.getWritersList().addAll(Arrays.asList(writers.split("\\s{2,2}")));

        movie.setStudio(eElement.getElementsByTagName("studio").item(0).getTextContent());
        
        return movie;
    }

    public String getMessage() {
        return message;
    }
    /**
     * Loop through the Document to find query
     *
     * @param title
     *
     * @return if title in Movies file.
     */
    
    public List<String> getByTitleList = new ArrayList<>();  
   
    public List<String> findMovieByTitle(String title){    
        readDocument();
            NodeList nList = doc.getElementsByTagName("movie");
            System.out.println(nList.getLength());
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;
                if (eElement.getElementsByTagName("title").item(0).getTextContent().equalsIgnoreCase(title)) {
                    getByTitleList.add(loadBean(eElement).toString());                    
                    System.out.println("The title list contains " + getByTitleList.get(0));                    
                    return getByTitleList;
                }
            }  
          return getByTitleList;
    }

    /**
     * Loop through the Document to find query
     *
     * @param actor
     * @return if Actor in Movies file.
     */
    public List<String> getByActorList = new ArrayList<>();  
    
    public List<String> findMovieByActorName(String actor) {

        readDocument();
        NodeList nList = doc.getElementsByTagName("movie");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            Element eElement = (Element) nNode;
            NodeList ns = (NodeList) eElement.getElementsByTagName("actor");
            for (int i = 0; i < ns.getLength(); i++) {
                if (ns.item(i).getTextContent().equalsIgnoreCase(actor)) {
                    getByActorList.add(loadBean(eElement).toString());
                    System.out.println("The actor list contains " + getByActorList.get(0));                                      
                    return getByActorList;
                }
            }
        }
        return getByActorList;
    }

    /**
     * Loop through the Document to find query
     *
     * @param director
     * @return if director in Movies file.
     */
    public List<String> getByDirectorList = new ArrayList<>();  
    
    public List<String> findMovieByDirectorName(String director) {
        readDocument();
        NodeList nList = doc.getElementsByTagName("movie");

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            Element eElement = (Element) nNode;
            NodeList ns = (NodeList) eElement.getElementsByTagName("director");
            for (int i = 0; i < ns.getLength(); i++) {
                if (ns.item(i).getTextContent().equalsIgnoreCase(director)) {
                    getByDirectorList.add(loadBean(eElement).toString());
                    System.out.println("The director list contains " + getByDirectorList.get(0));                                                         
                    return getByDirectorList;
                }
            }
        }
        return getByDirectorList;
    }   
}
